import React from 'react';
import image from '../Image/thankyou.png';

export default class ComingSoon extends React.Component {
    render() {
        const links = this.props.link ? this.props.link : "/";
        return(
            <div className="mt-40 sm:mt-[260px] text-center text-2xl sm:text-3xl">
                <img className="mx-auto w-[300px]" src={image} />
                <p className="text-lg"> <i>This Feature is under progress!</i> </p>
                <p className="text-lg font-semibold"> [ Coming Soon! ] </p>
                <a href={this.props.link}>
                    <button className="font-semibold text-sm mt-6 px-6 py-2 shadow-md rounded-sm bg-gray-800 text-white">
                        Go Back
                    </button>
                </a>
            </div>
        );
    };
}
