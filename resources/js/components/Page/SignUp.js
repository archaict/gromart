import { TextField } from '@mui/material';

import React from 'react';
import image from '../Image/login.png';

class SignUpHead extends React.Component {

    constructor(props) {
        super(props);

    }

    render = () => {
        return <div className="text-center">
                <p className="font-bold text-2xl"> Sign Up Page </p>
                <p className="text-sm"> <i>Please complete your registration!</i> </p>
            </div>;
    }
}

export default function SignUp() {
    return (

        <div className="flex h-screen">
            <div className="flex md:w-[60%] bg-gradient-to-r from-purple-500 to-pink-500">
            </div>

            <div className="flex md:w-[40%] h-screen w-full bg-slate-100">

                <div class="w-4/6 m-auto">

                    <SignUpHead />

                    <div className="w-[90%] mt-10 mx-auto">

                        <div className="mb-6"> <TextField label="Name *" color="grey" fullWidth /> </div>
                        <div className="mb-6"> <TextField label="Address *" color="grey" fullWidth /> </div>
                        <div className="mb-6"> <TextField label="Telephone Number *" color="grey" fullWidth /> </div>
                        <div className="mb-6"> <TextField label="Username *" color="grey" fullWidth /> </div>
                        <div className="mb-6"> <TextField label="Password *" color="grey" fullWidth /> </div>
                        <div className="mb-6"> <TextField label="Retype Password *" color="grey" fullWidth /> </div>

                        <a href="/">
                            <button className="inline-block px-7 py-3 bg-red-700 text-white font-semibold text-sm leading-snug uppercase rounded shadow-md hover:bg-red-800 hover:shadow-lg focus:bg-red-800 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-800 active:shadow-lg transition duration-150 ease-in-out w-full" >
                                Sign Up
                            </button>
                        </a>

                    </div>

                </div>
            </div>

        </div>


        // <div class="h-screen bg-gray-100">
        // </div>
    );
}
