import React from 'react';

import Footer from '../Layout/Footer';
import Navbar from '../Layout/Navbar';
import Blank from '../Layout/Blank';


export default function BlankPage () {
    return (
        <div className="bg-gray-100">
            <Navbar />

            <Blank/>

            <Footer />
        </div >
    );
}

