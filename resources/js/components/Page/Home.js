import React from 'react';

import BannerTop from '../Layout/Banner';
import Cards from '../Layout/Card';
import Footer from '../Layout/Footer';
import Navbar from '../Layout/Navbar';
import ProductCard from '../Layout/ProductCard';
import Sections from '../Layout/Sections';

class CardLayouts extends React.Component {
    render() {
        return (<Cards
            title='Anomaly Project'
            image='https:source.unsplash.com/random/300x300'
        />);
    }
}

class Container extends React.Component {
    render() {
        return (
            <div className='flex flex-wrap grid sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 2xl:grid-cols-4 grid-rows-1 gap-4' >
                <CardLayouts /> <CardLayouts /> <CardLayouts /> <CardLayouts />
            </div>
        );
    }
}

class ProductContainer extends React.Component {
    render() {
        return (
            <div class="grid grid-cols-2 sm:grid-cols-4 md:grid-cols-5 lg:grid-cols-6 xl:grid-cols-7 2xl:grid-cols-8 gap-8 mx-auto px-8">
                <ProductCard
                    productName='Anomaly Product'
                    productDescription='Lorem ipsum dolor sit amet.'
                    image='https:source.unsplash.com/random/300x300'
                />
                <ProductCard
                    productName='Anomaly Product'
                    productDescription='Lorem ipsum dolor sit amet.'
                    image='https:source.unsplash.com/random/300x300'
                />
                <ProductCard
                    productName='Anomaly Product'
                    productDescription='Lorem ipsum dolor sit amet.'
                    image='https:source.unsplash.com/random/300x300'
                />
                <ProductCard
                    productName='Anomaly Product'
                    productDescription='Lorem ipsum dolor sit amet.'
                    image='https:source.unsplash.com/random/300x300'
                />
                <ProductCard
                    productName='Anomaly Product'
                    productDescription='Lorem ipsum dolor sit amet.'
                    image='https:source.unsplash.com/random/300x300'
                />
                <ProductCard
                    productName='Anomaly Product'
                    productDescription='Lorem ipsum dolor sit amet.'
                    image='https:source.unsplash.com/random/300x300'
                />
                <ProductCard
                    productName='Anomaly Product'
                    productDescription='Lorem ipsum dolor sit amet.'
                    image='https:source.unsplash.com/random/300x300'
                />
                <ProductCard
                    productName='Anomaly Product'
                    productDescription='Lorem ipsum dolor sit amet.'
                    image='https:source.unsplash.com/random/300x300'
                />
            </div>
        );
    }
}


function Home() {
    return (
        <div className="bg-gray-100">
            <Navbar />

            <BannerTop />

            <Sections
                name="PRODUCT"
                sub="Lorem ipsum odor amet, consectetuer adipiscing elit."
            />
            <div className="">
                <ProductContainer />
            </div>

            <Sections
                name="CATALOG"
                sub="Lorem ipsum odor amet, consectetuer adipiscing elit."
            />
            <div className="container m-auto">
                <Container />
            </div>

            <Sections
                name="PROJECT // 03"
                sub="Lorem ipsum odor amet, consectetuer adipiscing elit."
            />
            <div className="container m-auto">
                <Container />
            </div>

            <Footer />

        </div >
    );
}

export default Home;
