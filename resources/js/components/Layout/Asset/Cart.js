import React from 'react';

export default class Cart extends React.Component {
    render() {
        const links = this.props.link ? this.props.link : "/product";
        return (
            <div class="flex items-end justify-end h-4 w-full">
                <a href={links}>
                    <button class="transition ease-in-out delay-150 hover:shadow-md p-2 rounded-full bg-red-700 text-white mx-5 -mb-4 hover:bg-red-800 focus:outline-none focus:bg-red-800">
                        <svg class="h-5 w-5" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path></svg>
                    </button>
                </a>
            </div>
        );
    };
}
