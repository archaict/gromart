import image from '../Image/bannerTest.jpg'

function PromoButton() {
    return (
        <>
            <button class="w-[160px] h-8 bg-black bg-opacity-60 rounded-md">
                <p class="text-sm text-white mt-[2px]">
                    lihat promo lainnya
                </p>
            </button>
        </>
    )
}

export default function BannerTop() {
    return (
        <div className="bg-gray-100">
            <div class="mx-auto px-8 relative flex items-center justify-center h-96 my-[20px]">
                <img class="object-cover w-full h-full max-w-full rounded-md"
                        src={image} />
                <div class='absolute right-[48px] bottom-[20px] '>
                    <PromoButton />
                </div>
            </div>
        </div>
    )
}

