import { CardContent, CardHeader, CardMedia, Typography } from '@mui/material';
import Card from '@mui/material/Card';
import image from '../Image/category.jpg';
import React from 'react';

import ButtonLink from './Asset/ButtonLink';
import Cart from './Asset/Cart';

// class Qr extends React.Component {
//     render() {
//         return (
//             <div>
//                 <svg
//                     className="w-6 h-6" fill="none" stroke="currentColor"
//                     viewBox="0 0 24 24" xmlns="http:www.w3.org/2000/svg" >
//                     <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
//                         d="M12 4v1m6 11h2m-6 0h-2v4m0-11v3m0 0h.01M12 12h4.01M16 20h4M4 12h4m12 0h.01M5 8h2a1 1 0 001-1V5a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1zm12 0h2a1 1 0 001-1V5a1 1 0 00-1-1h-2a1 1 0 00-1 1v2a1 1 0 001 1zM5 20h2a1 1 0 001-1v-2a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1z">
//                     </path>                                                                                                                                                                                                                                                                                    )
//                 </svg>
//             </div>
//         );
//     }
// }

class Cards extends React.Component {

    render() {
        const cardBg = 'hover:scale-105 max-w-[300px] min-w-[300px] mx-auto mt-16 hover:bg-[#eeeeee] bg-[#fafafa] shadow-md transition ease-in-out delay-150';
        const buttonBg = 'ml-[170px] text-white bg-indigo-800 px-6 py-2 mt-4 rounded-md hover:bg-indigo-500 transition ease-in-out';
        return (
            <div>
                <Card className={cardBg} >

                    <CardHeader
                        title={this.props.title}
                        subheader="September 14, 2016"
                    />

                    <CardMedia
                        component="img"
                        /* image={this.props.image} */
                        image={image}
                    />

                    <Cart />

                    <CardContent >

                        <Typography variant="body2" color="text.secondary">
                            This impressive paella is a perfect party dish and a fun meal to cook
                            together with your guests.
                        </Typography>

                        <ButtonLink name="More" link="/product" />

                    </CardContent>

                </Card>
            </div>
        );
    }
}


export default Cards;
